package API;

import io.restassured.response.Response;

import static common.Config.TOKEN;
import static io.restassured.RestAssured.given;

public class Employee {


    public void deleteUser(String empId){
   Response response= given()
            .header("Authorization", "Basic " + TOKEN)
            .pathParam("id", empId)
            .log().all()
            .when()
            .delete("employees/{id}")
            .then()
            .statusCode(200)
            .extract().response();
   System.out.println(response);
    }
}
