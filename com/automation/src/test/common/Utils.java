package common;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Calendar;
import java.util.Random;

public class Utils {

    /**
     * Generates a random string.
     *
     * @param length how many characters the string should be.
     * @return a randomly generated string `length` characters long.
     */
    public static String getRandomString(int length) {
        return RandomStringUtils.randomAlphabetic(length).toLowerCase();
    }

}