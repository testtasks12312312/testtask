package pages.webPages;

import common.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {
    private static  final PageElement userName = new PageElement(
            "User name field ",
            By.xpath("//input[@id=\"Username\"]"),
            false);

    private static  final PageElement password= new PageElement(
            "Password field",
            By.xpath("//input[@id=\"Password\"]"),
            true);
    private static  final PageElement logInButton= new PageElement(
            "Login button",
            By.xpath("//button[@type=\"submit\"]"),
            true);



    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean pageIsDisplayed() {
        return allRequiredElementDisplayed();
    }


    public void loginASAdmin(String userNameValue, String passwordValue){
        waitToBeClickable(userName);
        enterText(userName,userNameValue);
        enterText(password,passwordValue);
        click(logInButton);
    }
    

}
