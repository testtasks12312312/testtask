package pages.webPages;

import common.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EmployeePage extends BasePage{
    private static  final PageElement addButton = new PageElement(
            "Accept button",
            By.xpath("//button[@id=\"add\"]"),
            false);

    private static  final PageElement firstName = new PageElement(
            "First Name field",
            By.xpath("//input[@id=\"firstName\"]"),
            false);

    private static  final PageElement lastName = new PageElement(
            "Last Name field",
            By.xpath("//input[@id=\"lastName\"]"),
            false);

    private static  final PageElement dependents = new PageElement(
            "Dependents field",
            By.xpath("//input[@id=\"dependants\"]"),
            false);


    private static  final PageElement addButtonInModalWindow = new PageElement(
            "Add button  in the modal window",
            By.xpath("//button[@id=\"addEmployee\"]"),
            false);

    public EmployeePage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean pageIsDisplayed() {
        return false;
    }
    public void addUser(String firstNameValue,String lastNameValue,String dependentsValue){
        waitToBeClickable(addButton);
        click(addButton);
        enterText(firstName,firstNameValue);
        enterText(lastName,lastNameValue);
        enterText(dependents,dependentsValue);
        click(addButtonInModalWindow);

    }



      public boolean verificationEmployeeDisplayed (String firstNameValue, String lastNameValue){
          PageElement firstName = new PageElement(
                  "First name in the table ",
                  By.xpath("//tr//td[contains(text(),'" + firstNameValue + "')]"),
                  false);
          PageElement lastName = new PageElement(
                  "Last Name in the table",
                  By.xpath("//tr//td[contains(text(),'" + lastNameValue + "')]"),
                  false);
          waitToBePresent(firstName);

          return isElementPresent(firstName)&&isElementPresent(lastName);
      }

      public String returnIdByFirstName (String firstName){
          PageElement idByFirstName = new PageElement(
                  "Last Name in the table",
                  By.xpath("//tr//td[contains(text(),'" + firstName + "')]/preceding-sibling::td"),
                  false);
        return getText(idByFirstName);
      }

}
