package tests;

import common.driver.DriverFactory;
import io.restassured.RestAssured;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import pages.webPages.*;

import static common.Config.BASE_URL_API;


public class BaseTest {

    protected LoginPage loginPage;
    protected EmployeePage employeePage;
    public WebDriver driver;


    @BeforeMethod
    public void setupTestRun() {
        RestAssured.baseURI = BASE_URL_API;
        driver = new DriverFactory().getDriver();
        initPages();
    }
    @AfterMethod
    public void tearDown(){
    driver.close();
    driver.quit();

 }
    private void initPages(){
        loginPage = new LoginPage(driver);
        employeePage= new EmployeePage(driver);


    }
}
