package tests;


import API.Employee;
import common.Config;
import common.Utils;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static common.Config.BASE_PASSWORD;
import static common.Config.BASE_USERNAME;
import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertTrue;

public class EmployeeTest extends BaseTest {



    @Test(description = "Add user from UI and Delete from API", priority = 0)
    public void translateTestForDemo() {
        Employee employee= new Employee();
        String lastName = "LastName" + Utils.getRandomString(4);
        String firstName = "FirstName" + Utils.getRandomString(4);
        driver.get(Config.BASE_URL);
        loginPage.loginASAdmin(BASE_USERNAME, BASE_PASSWORD);
        employeePage.addUser(firstName, lastName, "4");
        assertTrue(employeePage.verificationEmployeeDisplayed(firstName, lastName));
        employee.deleteUser(employeePage.returnIdByFirstName(firstName));

    }



}